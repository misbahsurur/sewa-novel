<?php

use App\Http\Controllers\BerandaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NovelController;
use App\Http\Controllers\SewaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BerandaController::class,'index']);
Route::get('/detail/{id}', [BerandaController::class,'show'])->name('detail');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [BerandaController::class, 'dashboard']);
    Route::resource('sewa', SewaController::class);
    Route::group(['middleware' => 'cek_login:admin'], function () {
        Route::resource('novel', NovelController::class);
    });
    Route::group(['middleware' => 'cek_login:user'], function () {
        Route::post('sewa/konfirmasi/{id}',[SewaController::class, 'konfirmasi'])->name('konfirmasi');
        Route::post('sewa/kembalikan/{id}',[SewaController::class, 'kembalikan'])->name('kembalikan');
        Route::post('detail/sewa/{id}',[SewaController::class, 'store'])->name('sewa.sewa');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
