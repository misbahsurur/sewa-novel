@extends('layouts.master')
@inject('carbon', 'Carbon\Carbon')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Sewa Novel</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data Sewa Novel</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul</th>
                                        <th>Tanggal Sewa</th>
                                        <th>Tanggal Pengembalian</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 0; @endphp
                                    @foreach($data as $data)
                                    @php $no++; @endphp
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$data->judul}}</td>
                                        <td>{{$data->tgl_sewa}}</td>
                                        <td>{{$data->tgl_kembali}}</td>
                                        <td>{{$data->total}}</td>
                                        <td>{{$data->status}}</td>
                                        <td>
                                            @if ($data->status == 'Belum Bayar')
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default{{ $data->id }}">
                                                Bayar
                                            </button>
                                            @elseif ($carbon::now() >= $data->tgl_kembali && $data->status != 'Selesai')
                                            <form action="{{ route('kembalikan',$data->id) }}"method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-primary">Kembalikan</button>
                                            </form>
                                            @endif
                                            <div class="modal fade" id="modal-default{{ $data->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Konfirmasi Pembayaran Novel "{{$data->judul}}"</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ route('konfirmasi', $data->id) }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <label>Upload Bukti Pembayaran</label>
                                                            <input type="file" class="form-control @error('gambar') is-invalid @enderror" id="gambar" name="gambar" required>
                                                            @error('gambar')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                            @enderror
                                                        </div>
                                                        <div class="modal-footer justify-content-between">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                            <button type="submit" class="btn btn-success">Konfirmasi</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.card -->
                </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
