/*
 Navicat Premium Data Transfer

 Source Server         : KoneksiMisbah
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : sewanovel

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 31/07/2022 11:10:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_07_29_023539_create_novels_table', 2);
INSERT INTO `migrations` VALUES (6, '2022_07_30_111850_create_sewas_table', 3);

-- ----------------------------
-- Table structure for novel
-- ----------------------------
DROP TABLE IF EXISTS `novel`;
CREATE TABLE `novel`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `status` enum('Tersedia','Disewa') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of novel
-- ----------------------------
INSERT INTO `novel` VALUES (1, 'Laut Bercerita', 10000, 'Tersedia', 'Laut Bercerita.jpg', 'novel terbaru Leila S. Chudori, bertutur tentang kisah keluarga yang kehilangan, sekumpulan sahabat yang merasakan kekosongan di dada, sekelompok orang yang gemar menyiksa dan lancar berkhianat, sejumlah keluarga yang mencari kejelasan makam anaknya, dan tentang cinta yang tak akan luntur.', '2022-07-29 14:22:05', '2022-07-31 10:05:39');
INSERT INTO `novel` VALUES (2, 'Nanti Kita Cerita Tentang Hari Ini', 5000, 'Tersedia', 'Nanti Kita Cerita Tentang Hari Ini.jpg', 'Nanti Kita Cerita Tentang Hari Ini atau NKCTHI adalah sebuah novel flash fiction karya Marchella FP. NKCTHI merupakan kumpulan tulisan yang merefleksikan pengalaman personal banyak orang. Marchella menghimpun ribuan cerita dari berbagai sudut pandang.', '2022-07-29 14:35:12', '2022-07-31 09:56:41');
INSERT INTO `novel` VALUES (3, 'Autumn in Paris', 5000, 'Tersedia', 'Autumn in Paris.jpg', 'Tara Dupont menyukai Paris dan musim gugur. Ia mengira sudah memiliki segalanya dalam hidup… sampai ia bertemu Tatsuya Fujisawa yang susah ditebak dan selalu membangkitkan rasa penasarannya sejak awal. Tatsuya Fujisawa benci Paris dan musim gugur. Ia datang ke Paris untuk mencari orang yang menghancurkan hidupnya. Namun ia tidak menduga akan terpesona pada Tara Dupont, gadis yang cerewet tapi bisa menenangkan jiwa dan pikirannya... juga mengubah dunianya. Tara maupun Tatsuya sama sekali tidak menyadari benang yang menghubungkan mereka dengan masa lalu, adanya rahasia yang menghancurkan segala harapan, perasaan, dan keyakinan. Ketika kebenaran terungkap, tersingkap pula arti putus asa... arti tak berdaya... Kenyataan juga begitu menyakitkan hingga mendorong salah satu dari mereka ingin mengakhiri hidup.', '2022-07-29 14:39:05', '2022-07-30 12:57:10');
INSERT INTO `novel` VALUES (4, 'Kata', 10000, 'Tersedia', 'Kata.jpg', 'Banda Neira adalah hari-hari terakhirku bersamamu. Kutitipkan segala rindu, cerita, dan perasaan yang tak lagi kubawa, lewat sebuah ciuman perpisahan. Berjanjilah kau akan melanjutkan hidupmu bersama laki-laki yang bisa menjaga dan menyayangimu lebih baik dariku. Nugraha, Biru, dan Binta saling membelakangi dan saling pergi. Mereka butuh kata-kata untuk menjelaskan perasaan. Mereka harus bicara dan berhenti menyembunyikan kata hati serta mencari jawaban dari sebuah perasaan.', '2022-07-29 14:45:21', '2022-07-31 10:30:15');
INSERT INTO `novel` VALUES (5, 'Pulang', 12000, 'Tersedia', 'Pulang.jpg', '\"Aku tahu sekarang, lebih banyak luka di hati bapakku dibanding di tubuhnya. Juga mamakku, lebih banyak tangis di hati Mamak dibanding di matanya.\"\r\n\r\nSebuah kisah tentang perjalanan pulang, melalui pertarungan demi pertarungan, untuk memeluk erat semua kebencian dan rasa sakit.\"', '2022-07-29 14:47:13', '2022-07-30 18:48:55');
INSERT INTO `novel` VALUES (6, '7 Prajurit Bapak', 15000, 'Tersedia', '7 Prajurit Bapak.jpg', 'Bercerita tentang 7 anak dari seorang bapak pensiunan tentara yang memiliki mimpi dan tujuan hidup masing-masing. Namun, mereka kerap mendapat cibiran karena dianggap tidak melanjutkan budaya turun-temurun keluarga yang mengharuskan semua anak lelaki menjadi tentara.\r\nYoga—si anak tengah—yang sedari kecil dianggap tidak memiliki bakat apa pun, memantapkan mimpinya untuk menjadi seorang penulis. Meskipun, mimpinya itu sering kali ditolak olek kakak-kakaknya sendiri.', '2022-07-29 14:48:40', '2022-07-29 14:48:40');
INSERT INTO `novel` VALUES (7, '70 MIL', 5000, 'Tersedia', '70 MIL.jpg', 'Javio berumur lima belas tahun saat menyadari bahwa ia menyukai Primrose Charlotte Navari, gadis manis dengan mata hazel yang ia temui sepuluh tahun lalu di tengah padang bunga.\r\nJavio berumur delapan belas tahun saat ia berlutut di hadapan gadis itu, mengumpulkan semua murid di sekolah untuk menyaksikannya menyematkan cincin ke jari manis Primrose dan berjanji kepada dirinya sendiri untuk mencintai gadis itu selamanya.\r\nJavio berumur dua puluh lima tahun saat ia mengucap janji suci di penghujung altar. Di sebelahnya adalah Lilyan Charlotte Navari, kakak dari Primrose. Javio masih naïf, namun tidak bodoh. Ia tahu, Primrose tidak akan kembali.', '2022-07-29 14:50:47', '2022-07-29 14:50:47');
INSERT INTO `novel` VALUES (8, 'Geez & Ann #1 Untuk Peri Kecilku', 5000, 'Tersedia', 'Geez & Ann Untuk Peri Kecilku.jpg', 'Ann, dari kecil, aku susah sekali bicara banyak.\r\nNamun, sejak bertemu denganmu, aku ingin bisa banyak berkata-kata, khususnya saat bersamamu.\r\nAku tahu, aku jenis orang yang sedikit rumit. Namun, percayalah Ann, aku berusaha sekeras mungkin untuk bisa membuatmu memahamiku, walaupun aku tahu itu sulit.\r\nBerat sekali rasanya harus meninggalkanmu ke Berlin.\r\nHarus membiarkanmu sendirian dengan banyak pertanyaan. Kamu bisa sabar, kan? Tunggu ya, Geez akan pulang untuk Ann.', '2022-07-29 14:52:53', '2022-07-29 14:52:53');
INSERT INTO `novel` VALUES (9, 'Geez & Ann #2', 5000, 'Tersedia', 'Geez & Ann 2.jpg', 'Selalu untukmu Geez, Sebulan lebih aku sudah berada di Berlin. Mencari, mencari, dan mencari. Sampai akhirnya, aku menemukan sebuah jawaban yang terpaksa aku terima. Bukan jawaban terbaik, tetapi aku harus bisa menghargai itu. Berlin, aku titip dewa kejutanku, ya. Sembunyikan saja senjamu kalau dia nakal, pasti dia langsung jadi anak baik lagi. Berlin, aku juga titip rasa sayang ini untuknya. Seperti permintaannya, perasaan untuknya tidak akan lagi aku bawa pulang.', '2022-07-29 14:56:18', '2022-07-29 14:56:18');
INSERT INTO `novel` VALUES (10, 'Geez & Ann #3', 5000, 'Tersedia', 'Geez & Ann 3.jpg', 'Geez kembali dari Berlin dengan kondisi baik dan siap untuk melanjutkan ceritanya dengan Ann. Semua yang Geez kira akan mudah dan indah, ternyata berbeda dengan Ann yang sudah mulai belajar merapikan hidupnya dan memulai cerita baru tanpa Geez... sampai ketika Geez kembali hendak menjemput dan membawanya ke Berlin.\r\nSemua berubah, hati kecil Ann tetap ingin di Jogja karena mimpi dan cita-citanya ada di sana. Begitu pula dengan Geez yang tak bisa meninggalkan Berlin karena semua rencananya sudah tersusun rapi di sana.\r\nMereka berdua dihadapkan dua pilihan antara cinta dan cita-cita.', '2022-07-29 14:57:59', '2022-07-29 14:57:59');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for sewa
-- ----------------------------
DROP TABLE IF EXISTS `sewa`;
CREATE TABLE `sewa`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `novel_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `tgl_sewa` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `status` enum('Belum Bayar','Sudah Bayar','Selesai') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sewa
-- ----------------------------
INSERT INTO `sewa` VALUES (1, 5, 4, 20000, '2022-07-31', '2022-07-31', 'Selesai', 'Bukti1659237855.jpg', '2022-07-31 10:23:24', '2022-07-31 10:30:15');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` enum('admin','user') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$g4VcXjuU.Gk9CgxRdCMzxuCD9S1FQOFKk0V0uW6LKqoE6bS0ZXeXy', NULL, 'admin', '2022-07-30 10:06:01', '2022-07-30 10:06:01');
INSERT INTO `users` VALUES (4, 'Surur', 'surur@gmail.com', NULL, '$2y$10$C/n02M2eSBYTwxfsiskpXuh7OIzIcQZiw3Py4YzgJMUscv06UQIei', NULL, 'user', '2022-07-31 10:05:04', '2022-07-31 10:05:04');
INSERT INTO `users` VALUES (5, 'Misbah', 'misbah@gmail.com', NULL, '$2y$10$2j4Luw1pDwzMJar1TA.KVOGpy5u65POUxMz.T3cnMEkvqzst7IseO', NULL, 'user', '2022-07-31 10:22:48', '2022-07-31 10:22:48');

SET FOREIGN_KEY_CHECKS = 1;
