<?php

namespace App\Http\Controllers;

use App\Models\Sewa;
use App\Models\novel;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreSewaRequest;
use App\Http\Requests\UpdateSewaRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role == 'admin') {
            $data = Sewa::join('novel', 'novel.id','=','sewa.novel_id')
                ->join('users', 'users.id', '=', 'sewa.user_id')
                ->select('sewa.*','novel.judul', 'users.name')->get();
            return view('admin.sewa.index',compact('data'));
        } else {
            $data = Sewa::join('novel', 'novel.id','=','sewa.novel_id')
                    ->where('user_id', Auth::user()->id)
                    ->select('sewa.*','novel.judul')->get();
            return view('user.sewa.index',compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSewaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $num = $request->num;
        $harga = $request->harga;
        $total = $harga * $num;

        $sewa = new Sewa;
        $sewa->user_id = Auth::user()->id;
        $sewa->novel_id = $request->novel_id;
        $sewa->tgl_sewa = Carbon::now()->toDateString();
        $sewa->tgl_kembali = Carbon::now()->addDay($num)->toDateString();
        $sewa->status = 'Belum Bayar';
        $sewa->total = $total;
        $sewa->bukti = 'Belum bayar';
        $sewa->save();

        novel::where('id', $id)
            ->update([
                'status' => 'Disewa'
            ]);

        return redirect('sewa')->with('success', 'Silahkan Melakukan Pembayaran!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sewa  $sewa
     * @return \Illuminate\Http\Response
     */
    public function show(Sewa $sewa)
    {
        //
    }

    public function konfirmasi(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
        ];

        $this->validate($request, [
            'gambar' => 'required',
            'gambar.*' => 'mimes:jpg,jpeg,bmp,png,gif,svg',
        ], $message);
        $bukti = $request->file('gambar');
        $buktibayar = 'Bukti' .time(). '.' . $bukti->extension();
        $bukti->move(public_path('images'), $buktibayar);
        Sewa::where('id', $id)
            ->update([
                'bukti' => $buktibayar,
                'status' => 'Sudah Bayar',
            ]);
        return redirect('sewa')->with('success', 'Pembayaran berhasil!!!');
    }

    public function kembalikan($id)
    {
        $ids = Sewa::findOrFail($id);
        $novel = Sewa::where('id', $ids->id)
            ->update([
                'status' => 'Selesai',
            ]);
        novel::where('id', $ids->novel_id)
            ->update([
                'status' => 'Tersedia',
            ]);
        return redirect('sewa')->with('success', 'Novel sudah dikembalikan!!!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sewa  $sewa
     * @return \Illuminate\Http\Response
     */
    public function edit(Sewa $sewa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSewaRequest  $request
     * @param  \App\Models\Sewa  $sewa
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSewaRequest $request, Sewa $sewa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sewa  $sewa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sewa $sewa)
    {
        //
    }
}
