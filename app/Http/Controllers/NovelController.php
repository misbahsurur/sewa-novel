<?php

namespace App\Http\Controllers;

use App\Models\novel;
use App\Http\Requests\StorenovelRequest;
use App\Http\Requests\UpdatenovelRequest;
use Illuminate\Http\Request;

class NovelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = novel::all();
        return view('admin.novel.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.novel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorenovelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka'
        ];

        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
            'gambar' => 'required',
            'gambar.*' => 'mimes:jpg,jpeg,bmp,png,gif,svg',
        ], $message);

        $gambar = $request->file('gambar');
        $judul = $request->judul;
        $gambarnovel = $judul . '.' . $gambar->extension();
        $gambar->move(public_path('images'), $gambarnovel);
        novel::create($request->except(['gambar','status'])+[
            'status' => 'Tersedia',
            'gambar' => $gambarnovel
        ]);
        return redirect('novel')->with('success', 'Data novel berhasil ditambahkan!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\novel  $novel
     * @return \Illuminate\Http\Response
     */
    public function show(novel $novel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\novel  $novel
     * @return \Illuminate\Http\Response
     */
    public function edit(novel $novel)
    {
        return view('admin.novel.edit',compact('novel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatenovelRequest  $request
     * @param  \App\Models\novel  $novel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, novel $novel)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka'
        ];

        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
            'gambar.*' => 'mimes:jpg,jpeg,bmp,png,gif,svg',
        ], $message);

        if (!empty($request->file('gambar'))) {
            unlink(public_path('images') . '/' . $novel->gambar);
            $gambar = $request->file('gambar');
            $judul = $request->judul;
            $gambarnovel = $judul . '.' . $gambar->extension();
            $gambar->move(public_path('images'), $gambarnovel);

            novel::where('id', $novel->id)
                ->update($request->except(['gambar'])+[
                    'gambar' => $gambarnovel
                ]);
        }
        novel::where('id', $novel->id)
                ->update($request->except(['_token','_method']));
        return redirect('novel')->with('success', 'Data novel berhasil diubah!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\novel  $novel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = novel::findOrFail($id);
        unlink(public_path('images') . '/' . $data->gambar);
        $data->delete();
        return redirect('novel')->with('success', 'Data novel berhasil dihapus!!!');
    }
}
